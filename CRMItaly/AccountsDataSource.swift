//
//  AccountsDataSource.swift
//  CRMItaly
//
//  Created by 516756 on 23/11/15.
//  Copyright © 2015 516756. All rights reserved.
//

import Foundation

import UIKit

class AccountsDataSource: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate{
    
    var accounts = ["Account1", "Account2", "Account3"]
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accounts.count
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //accountField.text = accounts[row]
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return accounts[row]
    }
    
}

