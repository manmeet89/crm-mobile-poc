//
//  NativeViewController.swift
//  CRMItaly
//
//  Created by 516756 on 20/11/15.
//  Copyright © 2015 516756. All rights reserved.
//

import UIKit

class NativeViewController: UIViewController , UIPickerViewDataSource, UIPickerViewDelegate, UITextViewDelegate{
    
    var accounts = ["Account1", "Account2", "Account3"]
    var contacts = ["Contact1", "Contact2", "Contact3"]
    var accountsPicker = UIPickerView()
    var contactsPicker = UIPickerView()
   
    @IBOutlet var saveBtn: UIButton!
    @IBOutlet var clearBtn: UIButton!
    
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var meetingNotes: UITextView!
    @IBOutlet var accountField: UITextField!
    @IBOutlet var contactsField: UITextField!
    
    
    
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        accountsPicker.delegate = self
        accountsPicker.dataSource = self
        
        
        contactsPicker.delegate = self
        contactsPicker.dataSource = self
        
        meetingNotes.delegate = self
        
        accountField.inputView = accountsPicker
        contactsField.inputView = contactsPicker
        
        meetingNotes.layer.cornerRadius = 5
        meetingNotes.layer.masksToBounds = true
        
        saveBtn.layer.cornerRadius = 5
        saveBtn.layer.masksToBounds = true
        
        clearBtn.layer.cornerRadius = 5
        clearBtn.layer.masksToBounds = true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        if meetingNotes.text == "Enter meeting notes..." {
            meetingNotes.text = ""
            meetingNotes.textColor = UIColor.darkTextColor()
        }
        return true
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if(meetingNotes.text == "") {
            meetingNotes.textColor = UIColor.lightGrayColor()
            meetingNotes.text = "Enter meeting notes..."
        }
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // returns the # of rows in each component..
   
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == accountsPicker {
            return accounts.count
        } else {
            return contacts.count
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == accountsPicker {
            accountField.text = accounts[row]
            accountField.resignFirstResponder()
        } else {
            contactsField.text = contacts[row]
            contactsField.resignFirstResponder()
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == accountsPicker {
            return accounts[row]
        } else {
            return contacts[row]
        }
    }
    
    @IBAction func clearBtn(sender: AnyObject) {
        accountField.text = ""
        contactsField.text = ""
        meetingNotes.text = ""
        meetingNotes.textColor = UIColor.lightGrayColor()
        meetingNotes.text = "Enter meeting notes..."
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}