//
//  HybridWebViewController.swift
//  CRMItaly
//
//  Created by 516756 on 20/11/15.
//  Copyright © 2015 516756. All rights reserved.
//

import UIKit

class HybridWebViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet var webView: UIWebView!
    
    @IBOutlet var loading: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self

        // Do any additional setup after loading the view.
        let url = NSURL (string: "https://fil-tms.herokuapp.com/testResponsive");
        let requestObj = NSURLRequest(URL: url!);
        webView.loadRequest(requestObj);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func webViewDidStartLoad(webView: UIWebView) {
        loading.startAnimating()
    }
    func webViewDidFinishLoad(webView: UIWebView) {
        loading.stopAnimating()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
