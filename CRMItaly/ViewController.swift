//
//  ViewController.swift
//  CRMItaly
//
//  Created by 516756 on 20/11/15.
//  Copyright © 2015 516756. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var hybridBtn: UIButton!
    @IBOutlet var nativeBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        hybridBtn.layer.cornerRadius = 5
        hybridBtn.layer.masksToBounds = true
        
        
        nativeBtn.layer.cornerRadius = 5
        nativeBtn.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    


}

